const num1 = document.querySelector("#num1");                         //Associo l'oggetto dell'input1 alla costante num1 mediante selettore di classe.
const num2 = document.querySelector("#num2");                         //Associo l'oggetto dell'input2 alla costante num2 mediante selettore di classe.
const btnRsl = document.querySelector('.btnRsl');                     //Associo il bottone "=" alla costante "btnRsl" mediante selettore di classe. 
const btnOp = document.querySelectorAll('.btnOp');                    //Associo i 4 bottoni operazionali alla costante "btnOp" con il selettore di classe multiplo (questo creerà una nodeList).
const h5 = document.querySelector('#total');                          //Associo l'elemento h5 alla costante h5 mediante selettore di classe.
btnOp.forEach(p => {                                                  //Per ogni tasto operazionale:
    let res = 0;                                                      //inizializzo la variabile "res" a 0 per poter poi scrivere il risultato dell'operazione aritmetica.
    p.addEventListener('click', () => {                               //metto in ascolto al click per gli elementi "p" (la p servirà per distinguere il tasto selezionato da quelli non selezionati):
        if (p.hasAttribute('style')) {                                //se uno dei 4 tasti ha l'attributo "style", lo rimuovo e azzero i valori di input 1 e 2 (ciò è indispensabile per poter rendere 
            num1.value = "";                                          //deselezionabile un tasto già selezionato);   
            num2.value = "";
            p.removeAttribute('style');         
        } else {                                                      //altrimenti,
            btnOp.forEach(p => {                                      //prima di aggiungere un attributo "style" a un altro tasto,
                p.removeAttribute('style');                           //rimuovo tutti gli attributi "style" (ciò è indispensabile per poter disattivare     
            });                                                       //un tasto operazionale quando se ne clicca un altro, è una specie di refresh generalizzato sui 4 tasti),
            if (num1.value != "" && num2.value != "") {               //controllo se entrambi i valori di input1 e input2 sono stati inseriti,
                p.setAttribute('style' , 'border: 3px solid red;');   //aggiungo l'attributo "style" per creare il bordino rosso di selezione sul tasto selezionato.    
            }
        }
        switch (p.textContent) {                                      //switch case sul valore del contenuto testuale nell'oggetto del tasto "p" selezionato:
            case "+":                                                 //fai la somma se il tasto selezionato ha il contenuto testuale "+",
                res = num1.valueAsNumber + num2.valueAsNumber;
                break;
            case "-":                                                 //fai la sottrazione se il tasto selezionato ha il contenuto testuale "-",
                res = num1.valueAsNumber - num2.valueAsNumber;
                break;
            case "X":                                                 //fai la moltiplicazione se il tasto selezionato ha il contenuto testuale "X",
                res = num1.valueAsNumber * num2.valueAsNumber;
                break;
            case "/":                                                 //fai la divisione se il tasto selezionato ha il contenuto testuale "/".
                res = num1.valueAsNumber / num2.valueAsNumber;
                break;
            default:
                break;
        }
        num1.addEventListener('click', () => {                        //Metto in ascolto la casella di input1 sul click per poter resettare la calcolatrice
            num1.value = "";                                          //(necessario per poter cambiare i valori di input prima di aver terminato
            res = "";                                                 //il calcolo precedente).            
            p.removeAttribute('style');
        });
        num2.addEventListener('click', () => {                        //Metto in ascolto la casella di input2 sul click per poter resettare la calcolatrice
            num2.value = "";                                          //(necessario per poter cambiare i valori di input prima di aver terminato
            res = "";                                                 //il calcolo precedente).            
            p.removeAttribute('style');  
        });
        if (num1.value != "" && num2.value != "") {                   //Se entrambi gli input sono inseriti, allora: 
            btnRsl.addEventListener('click', () => {                  //metto in ascolto al click del tasto "=" per:
                //modificare il contenuto interno all'elemento "h5"
                //e aggiornare il suo valore su schermo,
                h5.innerHTML = `
                <h5>Result: ${res}</h5>
                `
                num1.value = "";                                      //azzerare il campo di input1 come stringa,
                num2.value = "";                                      //azzerare il campo di input2 come stringa,
                btnOp.forEach(p => {                                  //rimuovere la selezione del tasto di operazione.
                    p.removeAttribute('style');                             
                });
                btnRsl.addEventListener('click', () => {              //Metto in ascolto il click del tasto "=" per poter inserire il risulato
                    num1.value = res;                                 //dell'operazione precedente nel campo di input1 per poter effettuare                  
                });                                                   //l'operazione successiva in modo più rapido.
            });
        }             
    });
});
//NOTE: ho scritto il codice in modo che i tasti operazionali non possano essere attivati se prima
//non vengono riempiti entrambi i campi di input 1 e 2.
